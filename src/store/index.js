import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';

const baseURL = 'https://fakestoreapi.com'

const axiosConfig = {
  baseURL: baseURL,
  headers: {
    Accept: "application/json", "Content-Type": "application/json"
  },
};

const api = axios.create(axiosConfig)


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    searchValue: '',
    products: [],
    cart: [],
    token: localStorage.getItem('token') || null,
    username: localStorage.getItem('username') || null,
    password: localStorage.getItem('password') || null
  },
  mutations:{
    SET_SEARCH_VALUE_TO_VUEX: (state, value) => {
      state.searchValue = value;
    },
    SET_PRODUCTS_TO_STATE: (state, products) => {
      state.products = products;
    },
    SET_CART: (state, product) => {
      let isProductExists = false;
      if (state.cart.length) {
        state.cart.map(function (item) {
          if (item.title === product.title) {
            isProductExists = true;
            item.quantity++
          }
        })
        if (!isProductExists) {
          state.cart.push(product)
        }
      } else {
        state.cart.push(product)
      }
    },
    REMOVE_FROM_CART: (state, index) => {
      state.cart.splice(index, 1)
    },
    INCREMENT: (state, index) => {
      state.cart[index].quantity++
    },
    DECREMENT: (state, index) => {
      if (state.cart[index].quantity > 1) {
        state.cart[index].quantity--
      }
    },
    SET_TOKEN: (state, token) => {
      state.token = token
      localStorage.setItem('token', token)
    },
    SET_LOGOUT: (state) => {
      state.token = null
      localStorage.removeItem('token')
    }
  },
  actions: {
    GET_PRODUCTS_FROM_API({commit}) {
      return axios('https://fakestoreapi.com/products/', {
        method: "GET"
      })
        .then((products) => {
          commit('SET_PRODUCTS_TO_STATE', products.data);
          return products;
        })
        .catch((error) => {
          console.log(error)
          return error;
        })
    },
    GET_SEARCH_VALUE_TO_VUEX ({commit}, value) {
      commit('SET_SEARCH_VALUE_TO_VUEX', value)
    },
    ADD_TO_CART({commit}, product) {
      commit('SET_CART', product);
    },
    INCREMENT_CART_ITEM({commit}, index) {
      commit('INCREMENT', index)
    },
    DECREMENT_CART_ITEM({commit}, index) {
      commit('DECREMENT', index)
    },
    DELETE_FROM_CART({commit}, index) {
      commit('REMOVE_FROM_CART', index)
    },

    async GET_TOKEN({commit}, {username, password}){
      await api.post('auth/login', {
        username: username,
        password: password
      })
      .then(response => {
        let token = response.data.token;
        if (typeof token === 'undefined'){
          return Promise.reject(response)
        }
        else{
          commit("SET_TOKEN", token)
          console.log(response.data.token)
          return  Promise.resolve(response)
        }
      })
      .catch(error => {
        console.log(error)
        return Promise.reject(error)
      })
    },

    LOGOUT({commit}){
      commit('SET_LOGOUT')
    }
  },
  getters:{
    SEARCH_VALUE(state) {
      return state.searchValue;
    },
    PRODUCTS(state) {
      return state.products;
    },
    CART(state) {
      return state.cart;
    }
  }
    
    

})