import Vue from 'vue'
import Router from 'vue-router'

import vCatalog from '../components/catalog/v-catalog'
import vCart from '../components/cart/v-cart'
import vlogin from '../components/main-page/login'
import vProductPage from '../components/catalog/v-product-page'

Vue.use(Router);

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'vlogin',
      component: vlogin,
      meta: { layout: false },
    },
    {
      path: '/catalog',
      name: 'catalog',
      component: vCatalog,
      meta: { layout: true },
    },
    {
      path: '/product',
      name: 'product',
      component: vProductPage,
      meta: { layout: true },
    },
    {
      path: '/cart',
      name: 'cart',
      component: vCart,
      props: true,
      meta: { layout: true },
    }
  ]
})
  // const token = localStorage.getItem(token)

  router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('token');
  
    if (authRequired && !loggedIn) {
      return next('/');
    }
  
    next();
  })
export default router;

